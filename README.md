# MATCH BET BOT

Dans ce projet nous allons essayer de prédire le vainqueur d'un match de Football avant son lancement.<br><br>

>Pour l'instant nous nous avons réaliser un dashboard ineractif sur les performances des équipes. Nous implémenterons la partie prediction progressivement.<br>

Nous avons collecté les données en faisant du web scraping via **wget** et **python**.
- Dans **matchs-scraping.sh**, nous téléchargeons un ensemble de fichiers excels sur les matchs.<br>

- Dans **teams-scraping**, nous téléchargeons les liens des équipes et les sauvegardons dans le fichier filtered_links.txt.<br> Nous allons l'utiliser ensuite pour récupérer les données de chaque équipe contenue dans le fichier. Cette partie nous l'avons fait en python dans le notebook **pyton-scraping.ipynb**.<br> 
Dans ce dernier, il s'agit du code permettant de collecter les données sur les performances des équipes de Football.<br>
Pour tester, il faut utiliser la fonction get_data. Elle prend en entrée, les liens des équipes, un liste à mettre à jour, le nombre de saisons de données à collecter (min_season,max_season) et un fichier de sortie.

Comme la collecte met un certain temps, nous nous sommes amusés à le faire de manière distribuée à la main. Nous avons donc lancé sur chacun de nos pc le code suivant en utilisant chacun des intervalles [min_season,max_season] différents et en créant chacun un fichier team_skills-part-000N.json.<br>
```
teams_data=[]
f = open(filtered_links, "r")
links=[link.split('\n')[0] for link in f.readlines(-1)]
dataframe=get_data(links,teams_data,min_season=0,max_season=3,output_file='team_skills-part-0000.json')
```

Dans le notebook Datascience, il y a une partie **Data Science** et une partie **Visualisation**.<br>
